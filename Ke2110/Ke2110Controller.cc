#include "Ke2110Controller.h"

using namespace Ph2_UsbInst;

Ke2110Controller::Ke2110Controller () :
    fFileOpen (false),
    fReadInterval (5),
    fTemperatureFileName (""),
    fTemperatureFileOpen (false),
    fMonitoringRun (false),
    fServer (nullptr)

{
    fHandler = new UsbTmcHandler ("Keithley Instruments 2110 Multimeter");
    this->SystemBeeper (false);
    //clear the error queue
    LOG (INFO) << RED << "Read the following errors: " <<  this->SystemError() << RESET;
    fHandler->simplewrite ("*CLS\n");
    //std::string cReadString = fHandler->devwrite ( ("*IDN?") );

    //std::regex e ("(KEITHLEY INSTRUMENTS INC.,MODEL 2110,)(.*)");

    //if (std::regex_match (cReadString, e) )
    //LOG (INFO) << BOLDGREEN << "Successfully connected to Keithley 2110 Instrument" <<  RESET;

    //this->openLogFile();
    //this->initGraphs();
}

Ke2110Controller::Ke2110Controller (THttpServer* pServer) :
    fFileOpen (false),
    fReadInterval (5),
    fTemperatureFileName (""),
    fTemperatureFileOpen (false),
    fMonitoringRun (false),
    fServer (pServer)
{
    fHandler = new UsbTmcHandler ("Keithley Instruments 2110 Multimeter");
    this->SystemBeeper (false);
    //clear the error queue
    LOG (INFO) << RED << "Read the following errors: " <<  this->SystemError() << RESET;
    fHandler->simplewrite ("*CLS\n");
    //this->openLogFile();
    //this->initGraphs();
}


Ke2110Controller::~Ke2110Controller()
{
    //this->closeLogFile();
    //this->initGraphs();
    //if (fTempGraph) this->clearGraphs();

    if (fHandler) delete fHandler;
}

void Ke2110Controller::InitConfigFile (std::string pFilename)
{
    ConfigParser cParser;
    fConfig = cParser.parseKe2110 (pFilename);
}

void Ke2110Controller::Configure()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->simplewrite (fConfig.getFunction() );
    fHandler->simplewrite (fConfig.getRange() );

    if (!fConfig.fAutorange) fHandler->simplewrite (fConfig.getResolution() );

    fValues.fUnit = fConfig.getUnit();
}

void Ke2110Controller::Configure (std::string pFunction, double pRange, double pResolution, bool pAutorange)
{
    fConfig.fFunction = pFunction;
    fConfig.fRange = pRange;
    fConfig.fResolution = pResolution;
    fConfig.fAutorange = pAutorange;

    fValues.fUnit = fConfig.getUnit();

    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    //fHandler->simplewrite (fConfig.getConfig() );
    fHandler->simplewrite (fConfig.getFunction() );
    fHandler->simplewrite (fConfig.getRange() );

    if (!fConfig.fAutorange) fHandler->simplewrite (fConfig.getResolution() );
}

void Ke2110Controller::Measure()
{
    this->getTimestamp (fValues);
    std::unique_lock<std::mutex> guard (fInterfaceMutex);
    std::string cValueString = "";

    int counter = 0;

    while (cValueString.empty() )
    {
        cValueString = fHandler->devwrite ("READ?\n");

        if (counter++ > 5) LOG (ERROR) << "UsbTmc::devwrite timed out!";
    }

    std::this_thread::sleep_for (std::chrono::milliseconds (2) );

    guard.unlock();

    std::unique_lock<std::mutex> aguard (fMemberMutex);

    if (!cValueString.empty() )
        fValues.fValue = atof (cValueString.c_str() );
    else fValues.fValue = -999;

    aguard.unlock();

    this->PrintValues (fFile, fValues);
}

Measurement Ke2110Controller::GetLatestReadings()
{
    std::lock_guard<std::mutex> guard (fMemberMutex);
    return fValues;
}

double Ke2110Controller::GetLatestReadValue()
{
    std::lock_guard<std::mutex> guard (fMemberMutex);
    return fValues.fValue;
}

void Ke2110Controller::MeasureT()
{
    //get the timestamp for the temperature reading, lock members and interface
    std::unique_lock<std::mutex> guard (fInterfaceMutex);

    //read the values
    int counter = 0;
    std::string cValueString = "";

    //guard.lock();

    while (cValueString.empty() )
    {
        if (!fMonitoringPaused.load() )  cValueString = fHandler->devwrite ("MEAS:TCouple?\n");

        if (counter++ > 5) LOG (ERROR) << "UsbTmc::devwrite timed out!";
    }

    std::this_thread::sleep_for (std::chrono::milliseconds (2) );

    guard.unlock();
    this->getTimestamp (fTemperatureValues);
    std::unique_lock<std::mutex> aguard (fMemberMutex);

    if (!cValueString.empty() )
        fTemperatureValues.fValue = atof (cValueString.c_str() );
    else fTemperatureValues.fValue = -999;

    fTemperatureValues.fUnit = 'C';
    LOG (INFO) << "Measured " << fTemperatureValues.fValue << " C";
    aguard.unlock();
    this->PrintValues (fTemperatureFile, fTemperatureValues);
    this->fillGraphs();

    //re-apply the original configuration for synchronous measurements
    //this->Reset();
}

Measurement Ke2110Controller::GetLatestTReadings()
{
    std::lock_guard<std::mutex> guard (fMemberMutex);
    return fTemperatureValues;
}

double Ke2110Controller::GetLatestTValue()
{
    std::lock_guard<std::mutex> guard (fMemberMutex);
    return fTemperatureValues.fValue;
}

void Ke2110Controller::Reset()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->simplewrite ("*RST\n");
}

void Ke2110Controller::Autozero()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->simplewrite ("SENS:ZERO:AUTO ONCE\n");
}

//system functions
void Ke2110Controller::SystemBeeper (bool pState)
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    std::string cMsg = "SYST:BEEP:STATE ";
    cMsg += (pState) ? "ON\n" : "OFF\n";
    fHandler->simplewrite (cMsg);
}

std::string Ke2110Controller::SystemError()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    return fHandler->devwrite ("SYSTEM:ERROR?\n");
}

void Ke2110Controller::SystemCLS()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->simplewrite ("*CLS\n");
}

void Ke2110Controller::SystemLocal()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->simplewrite ("SYSTEM:LOCAL\n");
}

void Ke2110Controller::SystemRemote()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->simplewrite ("SYSTEM:REMOTE\n");
}

void Ke2110Controller::SystemGetInfo()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->devwrite ("SYSTEM:VERSION?\n");
}

void Ke2110Controller::StartMonitoring (uint32_t pReadInterval)
{
    this->SystemCLS();

    //only if the monitoring is not already running do something
    if (!fMonitoringRun.load() )
    {
        // set the fReadInterval member
        fReadInterval = pReadInterval;
        //open temperature logfile (the file access is mutexed) and assert mutex to change condition varialbe
        this->openTemperaturFile ();
        fMonitoringRun = true;
        fMonitoringPaused = false;
        fThread = std::thread (&Ke2110Controller::MonitoringWorkloop, this);
    }
    else
    {
        LOG (INFO) << "Monitoring already running, therfore doing nothing!";
        LOG (INFO) << "Just to be safe, re-open the log file";
        //this->PauseMonitoring();
        this->StopMonitoring();
        std::this_thread::sleep_for (std::chrono::seconds (fReadInterval.load() ) );
        this->StartMonitoring();
    }
}

void Ke2110Controller::StopMonitoring()
{

    //only do something if the monitoring is actually running
    if (fMonitoringRun.load() )
    {
        fMonitoringRun = false;
        fMonitoringPaused = false;

        //and join
        if (fThread.joinable() ) fThread.join();

        this->closeTemperatureFile();
    }
    else
    {
        LOG (INFO) << "Monitoring not running, therefore doing nothing!";
        LOG (INFO) << "Just to be safe, closing the log file!";
        this->closeTemperatureFile();
    }
}

void Ke2110Controller::MonitoringWorkloop ()
{
    while (fMonitoringRun.load() )
    {
        if (!fMonitoringPaused.load() )
        {
            //first, get the latest values in the vectors
            //this MeasureT() mutexes the access to the USBTMCHandler and fills the graphs
            this->MeasureT();
        }

        if (!fMonitoringRun.load() ) break;
        else
            // wait for timeout
            std::this_thread::sleep_for (std::chrono::seconds (fReadInterval.load() ) );
    }
}

void Ke2110Controller::getTimestamp (Measurement& pMeasurement)
{
    std::lock_guard<std::mutex> lock (fMemberMutex);
    pMeasurement.clear();
    pMeasurement.fTimestamp = std::time (nullptr);
}

void Ke2110Controller::PrintValues (std::ofstream& os, Measurement& pMeasurement)
{
    std::lock_guard<std::mutex> guard (fMemberMutex);
    os << pMeasurement.fTimestamp << "\t" << pMeasurement.fValue << " [" << pMeasurement.fUnit << "]" << std::endl;
}

void Ke2110Controller::openLogFile ()
{
    //std::lock_guard<std::mutex> guard (fMemberMutex);
    //fFile << std::unitbuf;
    fFile.open (fFileName.c_str(), std::fstream::app | std::fstream::out);

    if (!fFile.is_open() )
        //if (fFile.fail() )
        LOG (ERROR) << RED << "Error, could not open log file " << fFileName << RESET;
    else
    {
        this->getTimestamp (fValues);
        fFileOpen = true;
        LOG (INFO) << GREEN << "Successfully opened log file " << fFileName << RESET;
        fFile << "## File Format: ## " << std::endl;
        fFile << "## Timestamp\tReading [UNIT]" << std::endl;
        fFile << "Logfile opened " << fValues.fTimestamp << " which is " << this->formatDateTime (fValues) << std::endl;
    }
}
void Ke2110Controller::openTemperaturFile ()
{
    //std::lock_guard<std::mutex> guard (fMemberMutex);
    //fTemperatureFile << std::unitbuf;
    fTemperatureFile.open (fTemperatureFileName.c_str(), std::fstream::app | std::fstream::out);

    if (!fTemperatureFile.is_open() )
        //if (fTemperatureFile.fail() )
        LOG (ERROR) << RED << "Error, could not open Temperature log file " << fTemperatureFileName << RESET;
    else
    {
        this->getTimestamp (fTemperatureValues);
        fTemperatureFileOpen = true;
        LOG (INFO) << GREEN << "Successfully opened Temperature log file " << fTemperatureFileName << RESET;
        fTemperatureFile << "## File Format: ## " << std::endl;
        fTemperatureFile << "## Timestamp\tReading [UNIT]" << std::endl;
        fTemperatureFile << "Temperature Logfile opened " << fTemperatureValues.fTimestamp << " which is " << this->formatDateTime (fTemperatureValues) << std::endl;
    }
}

void Ke2110Controller::closeLogFile()
{
    //std::lock_guard<std::mutex> guard (fMemberMutex);

    if (fFileOpen)
    {
        this->getTimestamp (fValues);
        fFile << "Logfile closed " << fValues.fTimestamp << " which is " << this->formatDateTime (fValues) << std::endl;
        fFileOpen = false;
        fFile.close();
        LOG (INFO) << GREEN << "Closed log file " << fFileName << RESET;
    }
}

void Ke2110Controller::closeTemperatureFile()
{
    //std::lock_guard<std::mutex> guard (fMemberMutex);

    if (fTemperatureFileOpen)
    {
        this->getTimestamp (fTemperatureValues);
        fTemperatureFile << "Temperature Logfile closed " << fTemperatureValues.fTimestamp << " which is " << this->formatDateTime (fTemperatureValues) << std::endl;
        fTemperatureFileOpen = false;
        fTemperatureFile.close();
        LOG (INFO) << GREEN << "Closed Temperature log file " << fTemperatureFileName << RESET;
    }
}

void Ke2110Controller::initGraphs()
{
    //Graph for Temperature Monitoring
    fTempGraph = new TGraph();
    fTempGraph->SetName ("Temperature");
    fTempGraph->SetTitle ("Temperature");
    fTempGraph->SetLineColor (kOrange);
    fTempGraph->SetLineWidth (3);


    //fTempGraph->GetHistogram()->GetXaxis()->SetTitle ("Time [min]");
    //fTempGraph->GetHistogram()->GetYaxis()->SetTitle ("Temperature [C]");
    //fTempGraph->GetHistogram()->GetXaxis()->SetTimeDisplay (1);
    //fTempGraph->GetHistogram()->GetXaxis()->SetTimeFormat ("%H:%M");
    fTempGraph->Draw ("APL");

    if (fServer != nullptr)
        fServer->Register ("/", fTempGraph);

    LOG (INFO) << "Graph for Temperature Monitoring successfully created!";
}

void Ke2110Controller::fillGraphs()
{
    double xmin, ymin, xmax, ymax;
    std::unique_lock<std::mutex> guard (fMemberMutex);
    fTempGraph->SetPoint (fTempGraph->GetN(), fTemperatureValues.fTimestamp, fTemperatureValues.fValue);
    //sliding 30 min time window
    fTempGraph->GetHistogram()->GetXaxis()->SetLimits (fTemperatureValues.fTimestamp - 60 * 30, fTemperatureValues.fTimestamp + 2 * 60);
    guard.unlock();
    fTempGraph->ComputeRange (xmin, ymin, xmax, ymax);
    fTempGraph->SetMaximum (ymax + 5);
    fTempGraph->SetMinimum (ymin - 5);
    //absolute time window
    //fTempGraph->GetHistogram()->GetXaxis()->SetLimits (xmin - 60 * 2, xmax + 60 * 2);
    fTempGraph->GetHistogram()->GetXaxis()->SetTitle ("Time [min]");
    fTempGraph->GetHistogram()->GetYaxis()->SetTitle ("Temperature [C]");
    fTempGraph->GetHistogram()->GetXaxis()->SetTimeDisplay (1);
    fTempGraph->GetHistogram()->GetXaxis()->SetTimeFormat ("%H:%M");
    fTempGraph->GetHistogram()->GetXaxis()->SetNdivisions (503);
    fTempGraph->GetXaxis()->SetLabelSize (0.03);
    //fTempGraph->GetYaxis()->SetLabelSize (0.03);

    //fTempGraph->Draw ("APL");
}

void Ke2110Controller::clearGraphs()
{
    std::lock_guard<std::mutex> guard (fMemberMutex);

    if (fTempGraph) delete fTempGraph;
}

void Ke2110Controller::SetLogFileName (std::string pFilename)
{
    std::lock_guard<std::mutex> guard (fMemberMutex);
    fFileName = pFilename;
}
void Ke2110Controller::SetTemperatureLogFileName (std::string pFilename)
{
    std::lock_guard<std::mutex> guard (fMemberMutex);
    LOG (INFO) << RED << "Setting Temperatue Log File name to " << pFilename << RESET;
    fTemperatureFileName = pFilename;
}

void Ke2110Controller::InitializeClient (std::string pHostname, int pPort)
{
#ifdef __ZMQ__
    fClient = new Client (pHostname, pPort, "Ke2110");
    fClientInitialized = true;
    LOG (INFO) << GREEN << "Instantiated new Ke2110Controller::Client on host " << YELLOW << pHostname << GREEN << " port " << YELLOW << pPort << RESET;
#endif
}

bool Ke2110Controller::ClientInitialized()
{
#ifdef __ZMQ__
    return fClientInitialized;
#endif
    return false;
}

bool Ke2110Controller::SendMonitoringStop()
{
#ifdef __ZMQ__

    if (fClientInitialized)
    {
        std::string cRequest = "e";
        std::string cReply;
        bool cSuccess = fClient->SendRequestReceiveReply (cRequest, cReply);

        if (!cSuccess) return false;
        else if (cReply == PAUSE)
        {
            LOG (INFO) << "Monitoring stopped successfully";
            return true;
        }
        else return false;
    }
    else
    {
        LOG (ERROR) << "Client not initialized!";
        return false;
    }

#endif
}
bool Ke2110Controller::SendMonitoringStart()
{
#ifdef __ZMQ__

    if (fClientInitialized)
    {
        std::string cRequest = "s";
        std::string cReply;
        bool cSuccess = fClient->SendRequestReceiveReply (cRequest, cReply);

        if (!cSuccess) return false;
        else if (cReply == PAUSE)
        {
            LOG (INFO) << "Monitoring started successfully";
            return true;
        }
        else return false;
    }
    else
    {
        LOG (ERROR) << "Client not initialized!";
        return false;
    }

#endif
}

bool Ke2110Controller::SendPause()
{
#ifdef __ZMQ__

    if (fClientInitialized)
    {
        std::string cRequest = "p";
        std::string cReply;
        bool cSuccess = fClient->SendRequestReceiveReply (cRequest, cReply);

        if (!cSuccess) return false;
        else if (cReply == PAUSE)
        {
            LOG (INFO) << "PAUSE command processed successfully";
            return true;
        }
        else return false;
    }
    else
    {
        LOG (ERROR) << "Client not initialized!";
        return false;
    }

#endif
}
bool Ke2110Controller::SendResume()
{
#ifdef __ZMQ__

    if (fClientInitialized)
    {
        std::string cRequest = "r";
        std::string cReply;
        bool cSuccess = fClient->SendRequestReceiveReply (cRequest, cReply);

        if (!cSuccess) return false;
        else if (cReply == RESUME)
        {
            LOG (INFO) << "RESUME command processed successfully";
            return true;
        }
        else return false;
    }
    else
    {
        LOG (ERROR) << "Client not initialized!";
        return false;
    }

#endif
}

bool Ke2110Controller::SendQuit()
{
#ifdef __ZMQ__

    if (fClientInitialized)
    {
        std::string cRequest = "q";
        std::string cReply;
        bool cSuccess = fClient->SendRequestReceiveReply (cRequest, cReply);

        if (!cSuccess) return false;
        else if (cReply == QUIT)
        {
            LOG (INFO) << "QUIT command processed successfully";
            return true;
        }
        else return false;
    }
    else
    {
        LOG (ERROR) << "Client not initialized!";
        return false;
    }

#endif
}

bool Ke2110Controller::SendLogFileName (std::string pTemperatureFileName)
{
#ifdef __ZMQ__

    if (fClientInitialized)
    {
        std::string cRequest = "o " + pTemperatureFileName;
        std::string cReply;
        bool cSuccess = fClient->SendRequestReceiveReply (cRequest, cReply);

        if (!cSuccess) return false;
        else if (cReply.find (LOGFILE) != std::string::npos)
        {
            LOG (INFO) << "Changed log filename to " << pTemperatureFileName;
            return true;
        }
        else return false;
    }
    else
    {
        LOG (ERROR) << "Client not initialized!";
        return false;
    }

#endif
}
