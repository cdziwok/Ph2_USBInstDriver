#include "Client.h"
#ifdef __ZMQ__
using namespace Ph2_UsbInst;


bool Client::SendRequestReceiveReply (std::string& pRequest, std::string& pReply)
{
    this->newSocket();
    bool cSuccess = false;
    int cRetries_left = REQUEST_RETRIES;

    try
    {
        //  Send reply in ss back to client
        s_send (*fSocket, pRequest );
    }
    catch (zmq::error_t& e)
    {
        LOG (ERROR) << YELLOW << "Caught exception on send: " << e.what() << RESET;
    }

    bool cExpect_reply = true;

    while (cExpect_reply)
    {
        pReply = "";
        zmq::pollitem_t cItems[] = { {(void*)*fSocket, 0, ZMQ_POLLIN, 0} };

        try
        {
            //poll socket for reply with timeout
            zmq::poll (&cItems[0], 1, 2 * REQUEST_TIMEOUT * 1000); // so we can use Milliseconds in REQUEST_TIMEOUT
        }
        catch (zmq::error_t& e)
        {
            LOG (ERROR) << RED << "Caught exception on poll: " << e.what() << RESET;
        }

        //if we got a reply, process it
        if (cItems[0].revents & ZMQ_POLLIN)
        {
            try
            {
                pReply = s_recv (*fSocket);
            }
            catch (zmq::error_t& e)
            {
                LOG (ERROR) << RED << "Caught exception on read: " << e.what() << RESET;
            }

            cExpect_reply = false;
            cSuccess = true;
        }
        else if (--cRetries_left == 0)
        {
            LOG (ERROR) << BOLDRED << "Error, could not reach server " << fConnectionString << " - aborting after " << REQUEST_RETRIES << " retrys when sending message: " << BLUE << pRequest << BOLDRED << " !" << RESET;
            pReply = "";
            cExpect_reply = false;
            break;
        }
        else
        {
            LOG (ERROR) << BOLDYELLOW << "No response from server " << fConnectionString << " at attempt " << REQUEST_RETRIES - cRetries_left << ", retrying ..." << RESET;

            //old socket confused, so re-open one and re-send reply;
            this->newSocket();

            try
            {
                s_send (*fSocket, pRequest);
            }
            catch (zmq::error_t& e)
            {
                LOG (ERROR) << RED << "Caught exception on send: " << e.what() << RESET;
            }
        }
    }

    return cSuccess;
}

std::string Client::resolveHostname (std::string& pHostname)
{
    struct addrinfo hints, *results, *item;
    int status;
    char ipstr[INET6_ADDRSTRLEN];


    memset (&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;  /* AF_INET6 to force version */
    hints.ai_socktype = SOCK_STREAM;


    if ( (status = getaddrinfo (pHostname.c_str(), NULL, &hints, &results) ) != 0)
    {
        fprintf (stderr, "failed to resolve hostname \"%s\": %s", pHostname.c_str(), gai_strerror (status) );
        LOG (ERROR) << RED << "Failed to resolve hostname " << pHostname << RESET;
        return "error";
    }

    void* addr;
    char* ipver;

    for (item = results; item != NULL; item = item->ai_next)
    {
        /* get pointer to the address itself */
        /* different fields in IPv4 and IPv6 */
        if (item->ai_family == AF_INET) /* address is IPv4 */
        {
            struct sockaddr_in* ipv4 = (struct sockaddr_in*) item->ai_addr;
            addr = & (ipv4->sin_addr);
            ipver = "IPv4";
        }
        else  /* address is IPv6 */
        {
            struct sockaddr_in6* ipv6 = (struct sockaddr_in6*) item->ai_addr;
            addr = & (ipv6->sin6_addr);
            ipver = "IPv6";
        }


        /* convert IP to a string and print it */
        inet_ntop (item->ai_family, addr, ipstr, sizeof ipstr);
    }

    LOG (INFO) << BLUE << "Resolved hostname " << GREEN << pHostname << BLUE << " to " << ipver << ": " << YELLOW << ipstr << RESET;

    freeaddrinfo (results);
    return ipstr;
}

#endif
