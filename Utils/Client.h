
/*

    \file                          Client.h
    \brief                         Client class to communicate via TCP sockets provided by the ZeroMQ library
    \author                        Georg Auzinger
    \version                       1.0
    \date                          06/10/2016
    Support :                      mail to : georg.auzinger@SPAMNOT.cern.ch
 */

#ifndef _CLIENT_H__
#define _CLIENT_H__
#ifdef __ZMQ__
//the client code only makes sense if ZMQ is available
#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <zmq.hpp>
#include "../Utils/easylogging++.h"
#include "../Utils/zmqutils.h"
#include "../Utils/Utilities.h"
#include "../Utils/ConsoleColor.h"
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>

#define REQUEST_TIMEOUT 2000 //request timeout in ms
#define REQUEST_RETRIES 5    //number of retries


namespace Ph2_UsbInst {

    //class MeasurementValues;

    class Client
    {
      private:
        void newSocket()
        {
            if (fSocket) delete fSocket;

            fSocket = new zmq::socket_t (fContext, ZMQ_REQ);
            fSocket->connect (fConnectionString.c_str() );
            //configure socket not to wait at close time
            int cLinger = 0;
            fSocket->setsockopt (ZMQ_LINGER, &cLinger, sizeof (cLinger) );
        }

      protected:
        // connection string wiht IP and port of the client
        std::string fConnectionString;
        // zmq context and socket for requests to the server
        zmq::context_t fContext;
        zmq::socket_t* fSocket;

      public:
        //C-tor and D-tor
        Client (std::string pHostname, int pPort, std::string pType = "default") :
            fContext (1),
            fSocket (nullptr)
        {
            fConnectionString = "tcp://" + this->resolveHostname (pHostname) + ":" + std::to_string (pPort);

            if (pType == "default")
                LOG (INFO) << BLUE << "Connecting to Server at: " << BOLDGREEN << fConnectionString << RESET;
            else
                LOG (INFO) << BLUE << "Connecting to " << pType << " Server at: " << BOLDGREEN << fConnectionString << RESET;

            //this->newSocket();
        }
        ~Client()
        {
            if (fSocket) delete fSocket;
        }

        bool SendRequestReceiveReply (std::string& pRequest, std::string& pReply);
        std::string resolveHostname (std::string& pHostname);
    };

} // namespace Ph2_UsbInst



#endif
#endif
