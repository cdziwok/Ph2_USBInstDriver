/*

    \file                          Applock.h
    \brief                         Class to handle lockfiles for processes that should be unique on a host (lvSupervisor for example)
    \author                        Georg Auzinger
    \version                       1.0
    \date                          25/10/2016
    Support :                      mail to : georg.auzinger@SPAMNOT.cern.ch

 */

#ifndef __APPLOCK_H__
#define __APPLOCK_H__

#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/fcntl.h>
#include "../Utils/easylogging++.h"
#include "../Utils/ConsoleColor.h"

class AppLock
{

  private:
    int fLockDescriptor;
    bool fInfoFile;

  public:
    std::string fLockName;

    AppLock (std::string pLockName, std::string pMessage = "") : fLockName (pLockName), fLockDescriptor (-1), fInfoFile (false)
    {
        this->acquire_lock();

        if (fLockDescriptor > 0 && !pMessage.empty() )
        {
            this->write_info (pMessage);
            fInfoFile = true;
        }
    }

    ~AppLock()
    {

        if (fInfoFile)
            this->remove_info();

        this->release_lock();
    }

    std::string getLockName()
    {
        return fLockName;
    }

    int getLockDescriptor()
    {
        return fLockDescriptor;
    }

    int acquire_lock()
    {
        mode_t m = umask (0);
        fLockDescriptor = open (fLockName.c_str(), O_RDWR | O_CREAT, 0666);
        //TODO: write message in file
        umask (m);

        if (fLockDescriptor >= 0 && flock (fLockDescriptor, LOCK_EX | LOCK_NB) < 0)
        {
            //here I can not get a lock, therefore an instance is already running!
            close (fLockDescriptor);
            fLockDescriptor = -1;
            LOG (INFO) << BOLDRED << "ERROR, lock not acquired! Assuming process is already running" << RESET;
            //exit (1);
        }
        else LOG (INFO) << BOLDGREEN << "Created file lock at: " << fLockName << RESET;

        return fLockDescriptor;
    }

    void release_lock()
    {
        if (fLockDescriptor < 0)
            return;

        LOG (INFO) << BOLDGREEN << "Releasing lock at: " << fLockName << RESET;
        remove (fLockName.c_str() );
        close (fLockDescriptor);
        fLockDescriptor = -1;
    }

    void write_info (std::string pMessage)
    {
        std::string cInfoName = make_infoname();
        std::ofstream cFile (cInfoName);

        cFile << pMessage;
        cFile.close();
        LOG (INFO) << "Saving port information in file: " << cInfoName;
    }

    std::string get_info()
    {
        std::string cInfoName = make_infoname();
        std::ifstream cFile (cInfoName);

        if (cFile)
        {
            std::string cString;
            std::getline (cFile, cString);
            return cString;
        }
        else
        {
            LOG (ERROR) << "Info file " << cInfoName << " does not exist!";
            return "";
        }
    }

    void remove_info()
    {
        std::string cInfoName = make_infoname();

        if (remove (cInfoName.c_str() ) != 0)
            LOG (ERROR) << BOLDRED << "Error deleting info file " << cInfoName;
        else
            LOG (INFO) << "Infofile " << cInfoName << " successfully deleted!";
    }

  private:
    std::string make_infoname()
    {
        //first strip the .lock (if exists) from the lock file name
        std::string cInfoName = fLockName;

        if (cInfoName.find (".lock") )
            cInfoName = cInfoName.erase (fLockName.find ("."), std::string::npos);

        cInfoName += ".info";
        return cInfoName;
    }
};



#endif
