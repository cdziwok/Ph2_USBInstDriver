/*

    \file                          ConfigParser.h
    \brief                         Class to parse xml configuration files for USB instruments
    \author                        Georg Auzinger
    \version                       1.0
    \date                          22/09/2016
    Support :                      mail to : georg.auzinger@SPAMNOT.cern.ch

 */

#ifndef _CONFIGPARSER__
#define _CONFIGPARSER__
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <utility>
#include "../Utils/easylogging++.h"
#include "../Utils/pugixml.hpp"
#include "../Utils/Utilities.h"
#include "../Utils/ConsoleColor.h"

namespace Ph2_UsbInst {

    class ConfigParser
    {
      public:
        ConfigParser() {}
        ~ConfigParser() {}

        //std::vector<std::pair<double, double>> parseHMP4040 (std::string pFilename);
        std::vector<std::pair<double, double>> parseHMP4040 (std::string pFilename)
        {
            std::vector<std::pair<double, double>> cConfigVec (4, std::make_pair (0, 0) );

            if (pFilename.find (".xml") != std::string::npos)
            {
                pugi::xml_document cDoc;
                pugi::xml_parse_result cResult = cDoc.load_file (pFilename.c_str() );

                if (!cResult)
                {
                    LOG (ERROR) << RED << "Unable to open File " << pFilename << RESET ;
                    return cConfigVec;
                }

                LOG (INFO) << BOLDBLUE << "********************************************************************************" << RESET;
                LOG (INFO) << BOLDBLUE << "                        HMP 4040 Configuration: " << RESET;
                LOG (INFO) << BOLDBLUE << "********************************************************************************"  << RESET;

                pugi::xml_node cHMPnode = cDoc.child ("InstrumentDescription").child ("HMP4040");

                for (pugi::xml_node cChannelnode = cHMPnode.child ("Channel"); cChannelnode; cChannelnode = cChannelnode.next_sibling() )
                {
                    int cChannel = cChannelnode.attribute ("id").as_int();
                    //double cVoltage = convertAnyDouble (cChannelnode.attribute ("voltage").value() );
                    double cVoltage = cChannelnode.attribute ("voltage").as_double();
                    double cCurrent = cChannelnode.attribute ("current").as_double() ;

                    cConfigVec.at (cChannel - 1) = std::make_pair (cVoltage, cCurrent);
                    LOG (INFO) << "Channel: " << cChannel << " Voltage: " << cVoltage << " CurrentLimit: " << cCurrent ;
                }

                LOG (INFO) << BOLDBLUE << "********************************************************************************" << RESET << std::endl ;

            }
            else
                LOG (ERROR) << RED << "Error: could not parse Settings file for HMP4040 - it is not .xml!" << RESET;

            return cConfigVec;
        }

        InstConfig parseKe2110 (std::string pFilename)
        {
            InstConfig cConfig;

            if (pFilename.find (".xml") != std::string::npos)
            {
                pugi::xml_document cDoc;
                pugi::xml_parse_result cResult = cDoc.load_file (pFilename.c_str() );

                if (!cResult)
                {
                    LOG (ERROR) << RED << "Unable to open File " << pFilename << RESET;
                    return cConfig;
                }

                LOG (INFO) << BOLDBLUE <<  "********************************************************************************" << RESET;
                LOG (INFO) << BOLDBLUE << "                        Ke 2110 Configuration: " << RESET;
                LOG (INFO) << BOLDBLUE << "********************************************************************************"  << RESET;

                pugi::xml_node cKEnode = cDoc.child ("InstrumentDescription").child ("Ke2110");

                pugi::xml_node cFunctionNode = cKEnode.child ("Function");
                cConfig.fFunction = cFunctionNode.attribute ("value").as_string();

                pugi::xml_node cRangeNode = cKEnode.child ("Range");
                cConfig.fRange = cRangeNode.attribute ("value").as_double();

                if (cConfig.fRange == 0) cConfig.fAutorange = true;

                pugi::xml_node cResolutionNode = cKEnode.child ("Resolution");
                cConfig.fResolution = cResolutionNode.attribute ("value").as_double();

                LOG (INFO) << "Function: " << cConfig.fFunction;
                LOG (INFO) << "Range: " << cConfig.fRange << " Autorange: " << cConfig.fAutorange;
                LOG (INFO) << "Resolution: " << cConfig.fResolution;
                LOG (INFO) << "Unit: " << cConfig.getUnit();

                LOG (INFO) << BOLDBLUE << "********************************************************************************"  << RESET << std::endl ;

            }
            else
                LOG (ERROR) << RED << "Error: could not parse Settings file for Ke 2110 - it is not .xml!" << RESET;

            return cConfig;
        }

      private:
        uint32_t convertAnyInt ( const char* pRegValue )
        {
            if ( std::string ( pRegValue ).find ( "0x" ) != std::string::npos ) return static_cast<uint32_t> ( strtoul ( pRegValue , 0, 16 ) );
            else return static_cast<uint32_t> ( strtoul ( pRegValue , 0, 10 ) );
        }

        uint32_t convertAnyDouble ( const char* pRegValue )
        {
            return static_cast<double> ( std::stod ( pRegValue , nullptr) );
        }
    };
}
#endif
