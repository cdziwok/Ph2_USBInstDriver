/*
  Super simple switch on LED 
*/

int ledPin = 13; 
int ledDelay = 5;
int pollDelay_ms = 1 ;
int relay1Pin = 2;
int relay2Pin = 3;

String inputString;
boolean stringComplete ; 

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 57600 bits per second:
  Serial.begin(9600);
  //Serial.flush();
   
  // intiialize pin connected to relay as an output
  pinMode(relay1Pin, OUTPUT);
  pinMode(relay2Pin, OUTPUT);

  // insert delay here 
  // to allow computer to do what it wants
  //delay(100); 
  
  Serial.print("ARDUINO NANO READY\n");
  inputString = "";
  stringComplete = false; 
 
}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 Shamelessly taken from : https://www.arduino.cc/en/Tutorial/SerialEvent
 and modified to accept SCPI like commands.
 */
void serialEvent() 
{
  if (Serial.available()) 
  {
    // get the new byte:
    char inChar = (char)Serial.read();
    //Serial.print(inChar); //Serial.print("\n");
    // clear string at the start of the command ( indicated by * )
   
    // add it to the inputString if its not an EOL character : 
    if( inChar != '\n') inputString += (char)inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    else if (inChar == '\n') {
      stringComplete = true;
    }
    
    // every time this executes I want to see the arduino blink
    //digitalWrite(ledPin, HIGH);
    //delay(1000); 
    //digitalWrite(ledPin, LOW);
    
  }
}

void parser()
{
  if( inputString == "LED:SEL 1") digitalWrite(ledPin, HIGH);
  if( inputString == "LED:SEL 0") digitalWrite(ledPin, LOW);
  if( inputString == "REL:SEL 1"){ digitalWrite(relay1Pin, HIGH) ; digitalWrite(relay2Pin, LOW) ; digitalWrite(ledPin, HIGH) ;}
  if( inputString == "REL:SEL 0"){ digitalWrite(relay1Pin, LOW) ;  digitalWrite(relay2Pin, HIGH) ; digitalWrite(ledPin, LOW) ;}
  if( inputString == "REL?"){ inputString = digitalRead(relay1Pin); }
}

char buffer[128];
int j = 0;
// the loop routine runs over and over again forever:
void loop() 
{
  //Serial.print("ARDUINO NANO READY\n");
  serialEvent();
  if( stringComplete ) 
  {
    parser();
    Serial.print(inputString + "\n");
    inputString = "";
    stringComplete = false;
  }
}
