ZMQINSTALLED = no
ZMQ_HEADER_PATH = /usr/include/zmq.hpp
ZmqFlag = -D__ZMQ__
HttpFlag = -D__HTTP__
ROOTVERSION := $(shell root-config --has-http)
DEPENDENCIES := Drivers Utils

##################################################
## check if the Root has THttp
##################################################
ifneq (,$(findstring yes,$(ROOTVERSION)))
	ExtObjectsRoot += $(RootLibraryPaths) -lRHTTP $(HttpFlag)
else
	ExtObjectsRoot += $(RootLibraryPaths) 
endif

##################################################
## check if ZMQ library is available on this machine
##################################################

#$(info $$ZMQ_HEADER_PATH is [${ZMQ_HEADER_PATH}])

ifneq ("$(wildcard $(ZMQ_HEADER_PATH))","")
	ExternalObjects += -lzmq $(ZmqFlag)
	ZMQINSTALLED = yes
else
	ZMQINSTRUCTIONS = ZeroMQ library is required to build the lvSupervisor with the server option - get it with \'sudo yum install cppzmq-devel\'
endif

.PHONY: print dependencies $(DEPENDENCIES) clean HMP4040 Ke2110 ArdNano

all: src
	@echo ExternalObjects is $(ExternalObjects)
	@echo ZMQINSTALLED = $(ZMQINSTALLED)

dependencies: print $(DEPENDENCIES)

$(DEPENDENCIES):
	$(MAKE) -C $@

HMP4040: dependencies
	$(MAKE) -C $@

Ke2110: dependencies
	$(MAKE) -C $@

ArdNano: dependencies
	$(MAKE) -C $@

src: dependencies HMP4040 Ke2110 ArdNano
	$(MAKE) -C $@

print:
	@echo '   '   
	@echo '*****************************'
	@echo 'BUILDING PH2 USB Instrument Library'
	@echo 'with the following Dependencies:'
	@echo $(DEPENDENCIES)
	@echo '*****************************'
	@echo 'Root Has Http:' $(ROOTVERSION)
	@echo '*****************************'
	@echo 'ZeroMQ library intalled:' $(ZMQINSTALLED)
	@echo $(ZMQINSTRUCTIONS)
	@echo '*****************************'


clean:
	(cd HMP4040; make clean)
	(cd Ke2110; make clean)
	(cd ArdNano; make clean)
	(cd Utils; make clean)
	(cd Drivers; make clean)
	(cd usbtmc; make clean)
	(rm -f lib/* bin/*)
