;
#include "HMP4040Controller.h"

using namespace Ph2_UsbInst;

HMP4040Controller::HMP4040Controller() :
    //fValues.fVoltages (4 , 0),
    //fCurrentVec (4 , 0),
    fConfigVec (4, std::make_pair (0, 0) ),
    fReadInterval (2),
    fFileName (""),
    fFileOpen (false),
    fMonitoringRun (false),
    fMonitoringPaused (false),
    fServer (nullptr)
{
    //struct to hold the latest measurement values
    //fValues.fVoltages (4, 0);
    //fValues.fCurrents (4, 0);

    fHandler = new SerialHandler ("HMP4040");
    std::string cReadString = fHandler->devwrite (formatString ("*IDN?") );

    std::regex e ("(HAMEG,HMP4040,)(.*)");

    if (std::regex_match (cReadString, e) )
        LOG (INFO) << BOLDGREEN << "Successfully connected to HMP4040 Instrument" <<  RESET;

    //reset the instrument??
    //this->Reset();

    this->initGraphs();
}

HMP4040Controller::HMP4040Controller (THttpServer* pServer) :
    //fVoltageVec (4 , 0),
    //fCurrentVec (4 , 0),
    fConfigVec (4, std::make_pair (0, 0) ),
    fReadInterval (2),
    fFileName (""),
    fFileOpen (false),
    fMonitoringRun (false),
    fMonitoringPaused (false),
    fServer ( pServer )
{
    //struct to hold the latest measurement values
    //fValues.fVoltages (4, 0);
    //fValues.fCurrents (4, 0);

    fHandler = new SerialHandler ("HMP4040");
    std::string cReadString = fHandler->devwrite (formatString ("*IDN?") );

    std::regex e ("(HAMEG,HMP4040,)(.*)");

    if (std::regex_match (cReadString, e) )
        LOG (INFO) << BOLDGREEN << "Successfully connected to HMP4040 Instrument" << RESET ;

    //reset the instrument??
    //this->Reset();
    this->initGraphs();
}

HMP4040Controller::~HMP4040Controller()
{
    this->clearGraphs();

    if (fHandler) delete fHandler;
}

void HMP4040Controller::InitConfigFile (std::string pFilename)
{
    ConfigParser cParser;
    fConfigVec = cParser.parseHMP4040 (pFilename);
}

void HMP4040Controller::Configure()
{
    //this->Reset();
    std::this_thread::sleep_for (std::chrono::milliseconds (500) );
    this->SystemRemote();
    this->ApplyConfig();
}

void HMP4040Controller::Reset()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->devwrite (formatString ("*RST") );
}

void HMP4040Controller::SelectChannel (uint32_t pChannel)
{
    if (pChannel < 5 && pChannel > 0)
    {
        std::lock_guard<std::mutex> guard (fInterfaceMutex);
        fHandler->devwrite (formatString ("INST:NSEL " + std::to_string (pChannel) ) );
    }
    else
        LOG (ERROR) << RED << "Error, selected channel must be between 1 and 4 " << RESET;
}

int HMP4040Controller::GetChannelAcitve()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    int cChannel = atoi ( fHandler->devwrite (formatString ("INST:NSEL?") ).c_str() );
    fHandler->devread();
    return cChannel;
}

void HMP4040Controller::ConfigChannel (uint32_t pChannel, double pVoltage, double pCurrent)
{
    this->SelectChannel (pChannel);
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->devwrite (formatString ("APPLY " + std::to_string (pVoltage) + "," + std::to_string (pCurrent) ) );
}

void HMP4040Controller::ConfigVoltage (uint32_t pChannel, double pVoltage)
{
    this->SelectChannel (pChannel);
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->devwrite (formatString ("VOLT " + std::to_string (pVoltage) ) );
}

void HMP4040Controller::ConfigCurrent (uint32_t pChannel, double pCurrent)
{
    this->SelectChannel (pChannel);
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->devwrite (formatString ("CURR " + std::to_string (pCurrent) ) );
}

void HMP4040Controller::SetOutputState (uint32_t pChannel, bool pState)
{
    this->SelectChannel (pChannel);
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->devwrite (formatString ("OUTP:SEL " + std::to_string (pState) ) );
}

bool HMP4040Controller::GetOutputState (uint32_t pChannel)
{
    this->SelectChannel (pChannel);
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    bool cOutputState = static_cast<bool> (atoi (fHandler->devwrite (formatString ("OUTP?") ).c_str() ) );
    fHandler->devread();
    return cOutputState;
}

void HMP4040Controller::MeasureVoltage (uint32_t pChannel)
{
    this->SelectChannel (pChannel);
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    std::lock_guard<std::mutex> aguard (fMemberMutex);
    fValues.fVoltages.at (pChannel - 1) = atof (fHandler->devwrite (formatString ("MEAS:VOLT?") ).c_str() );
    fHandler->devread();
}

void HMP4040Controller::MeasureCurrent (uint32_t pChannel)
{
    this->SelectChannel (pChannel);
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    std::lock_guard<std::mutex> aguard (fMemberMutex);
    fValues.fCurrents.at (pChannel - 1) = atof (fHandler->devwrite (formatString ("MEAS:CURR?") ).c_str() );
    fHandler->devread();
}

MeasurementValues HMP4040Controller::GetLatestReadValues()
{
    //std::vector<std::pair<double, double>> cValues;

    std::lock_guard<std::mutex> guard (fMemberMutex);

    //for (int i = 0; i < 4; i++)
    //cValues.push_back (std::make_pair (this->fVoltageVec.at (i), this->fCurrentVec.at (i) ) );

    //return cValues;
    return fValues;
}

void HMP4040Controller::ApplyConfig()
{
    // applies config from config vector and toggles the output on
    for (uint32_t iChan = 0; iChan < 4; iChan++)
    {
        LOG(INFO) << BOLDBLUE << "Applying configure for channel " << +iChan << RESET; 
        this->ConfigChannel (iChan + 1, fConfigVec.at (iChan).first, fConfigVec.at (iChan).second);
        sleep(2);
        LOG(INFO) << BOLDBLUE << "Turning on channel " << +iChan << RESET; 
        this->SetOutputState (iChan + 1, true);
        sleep(2);
        LOG(INFO) << BOLDBLUE << "Configured channel " << +iChan << RESET; 

    }
}

void HMP4040Controller::MeasureAll()
{
    this->getTimestamp();

    for (uint32_t iChan = 0; iChan < 4; iChan++)
    {
        this->MeasureVoltage (iChan + 1 );
        this->MeasureCurrent (iChan + 1 );
    }

    if (fFileOpen)
        this->PrintValues (fFile);

    std::unique_lock<std::mutex> cGuard (fMemberMutex);
    LOG (INFO) << fValues.fTimestamp << "\t" << fValues.fVoltages.at (0) << "\t" << fValues.fCurrents.at (0) << "\t" << fValues.fVoltages.at (1) << "\t" << fValues.fCurrents.at (1) << "\t" << fValues.fVoltages.at (2) << "\t" << fValues.fCurrents.at (2) << "\t" << fValues.fVoltages.at (3) << "\t" << fValues.fCurrents.at (3);
    cGuard.unlock();

    this->fillGraphs();
}

void HMP4040Controller::ToggleGlobalOutput (bool pState)
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->devwrite (formatString ("OUTP:GEN " + std::to_string (pState) ) );
}

void HMP4040Controller::SystemBeeper()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->devwrite (formatString ("SYST:BEEP:IMM") );
}

std::string HMP4040Controller::SystemError()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    std::string cError = fHandler->devwrite (formatString ("SYST:ERR?") );
    fHandler->devread();
    return cError;
}

void HMP4040Controller::SystemLocal()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->devwrite (formatString ("SYST:LOC") );
}

void HMP4040Controller::SystemRemote()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->devwrite (formatString ("SYST:REM") );
}

void HMP4040Controller::SystemMix()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    fHandler->devwrite (formatString ("SYST:MIX") );
}

void HMP4040Controller::SystemGetInfo()
{
    std::lock_guard<std::mutex> guard (fInterfaceMutex);
    LOG (INFO) << YELLOW << fHandler->devwrite (formatString ("SYST:VERS?") ) << RESET;
    fHandler->devread();
}

void HMP4040Controller::openLogFile ()
{
    //std::lock_guard<std::mutex> guard (fMemberMutex);
    //fFile.rdbuf()->pubsetbuf (0, 0);
    //fFile << std::unitbuf;
    fFile.open (fFileName.c_str(), std::fstream::app |  std::fstream::out);

    //if (fFile.fail() )
    if (!fFile.is_open() )
        LOG (ERROR) << RED << "Error, could not open log file " << fFileName << RESET;
    else
    {
        this->getTimestamp();
        fFileOpen = true;
        LOG (INFO) << GREEN << "Successfully opened log file " << fFileName  << RESET;
        fFile << "## File Format: ## " << std::endl;
        fFile << "## Timestamp\tVoltageCh1\tCurrentCh1\tVoltageCh2\tCurrentCh2\tVoltageCh3\tCurrentCh3\tVoltageCh4\tCurrentCh4 ##" << std::endl;
        fFile << "Logfile opened " << fValues.fTimestamp << " which is " << this->formatDateTime() << std::endl;

    }
}

void HMP4040Controller::closeLogFile()
{
    //std::lock_guard<std::mutex> guard (fMemberMutex);

    if (fFileOpen)
    {
        this->getTimestamp();
        fFile << "Logfile closed " << fValues.fTimestamp << " which is " << this->formatDateTime() << std::endl;
        fFileOpen = false;
        fFile.close();
        LOG (INFO) << GREEN << "Closed log file " << fFileName << RESET ;
    }
}

void HMP4040Controller::SetLogFileName (std::string pFilename)
{
    std::lock_guard<std::mutex> guard (fMemberMutex);
    fFileName = pFilename;
}

void HMP4040Controller::StartMonitoring (uint32_t pReadInterval)
{
    //if the monitoring is already running, do nothing
    if (!fMonitoringRun.load() )
    {
        // set the fReadInterval member
        fReadInterval = pReadInterval;
        //open logfile (the file access is mutexed) and assert mutex to change condition varialbe
        this->openLogFile ();
        fMonitoringRun = true;
        fMonitoringPaused = false;
        fThread = std::thread (&HMP4040Controller::MonitoringWorkloop, this);
    }
    else
    {
        LOG (INFO) << "Monitoring already running, therefore doing nothing!";
        LOG (INFO) << "Just to be safe, closing and re-opening the log file!";
        //this->PauseMonitoring();
        this->StopMonitoring();
        std::this_thread::sleep_for (std::chrono::seconds (fReadInterval.load() ) );
        //this->closeLogFile();
        //this->openLogFile();
        //std::this_thread::sleep_for (std::chrono::seconds (fReadInterval.load() ) );
        //this->ResumeMonitoring();
        this->StartMonitoring();
    }
}

void HMP4040Controller::StopMonitoring()
{
    //if the monitoring is already stopped, do nothing
    if (fMonitoringRun.load() )
    {
        fMonitoringRun = false;
        fMonitoringPaused = false;
        //give the workloop in the other thread some time to finish
        //std::this_thread::sleep_for (std::chrono::seconds (2 * fReadInterval.load() ) );

        if (fThread.joinable() ) fThread.join();

        this->closeLogFile();
    }
    else
    {
        LOG (INFO) << "Monitoring not running, therefore doing nothing!";
        LOG (INFO) << "Just to be safe, closing the log file!";
        this->closeLogFile();
    }
}

void HMP4040Controller::MonitoringWorkloop ()
{
    while (fMonitoringRun.load() )
    {
        if (!fMonitoringPaused.load() )
        {
            //this MeasureAll() mutexes the access to the USBTMCHandler and fills the graphs
            this->MeasureAll();
        }

        if (!fMonitoringRun.load() ) break;
        else
            std::this_thread::sleep_for (std::chrono::seconds (fReadInterval.load() ) );
    }
}

void HMP4040Controller::getTimestamp()
{
    std::lock_guard<std::mutex> guard (fMemberMutex);
    fValues.fTimestamp = std::time (nullptr);
}

void HMP4040Controller::PrintValues (std::ofstream& os)
{
    std::lock_guard<std::mutex> guard (fMemberMutex);
    os << fValues.fTimestamp << "\t" << fValues.fVoltages.at (0) << "\t" << fValues.fCurrents.at (0) << "\t" << fValues.fVoltages.at (1) << "\t" << fValues.fCurrents.at (1) << "\t" << fValues.fVoltages.at (2) << "\t" << fValues.fCurrents.at (2) << "\t" << fValues.fVoltages.at (3) << "\t" << fValues.fCurrents.at (3) << std::endl;
}

void HMP4040Controller::initGraphs()
{
    //Create the TMultiGraph and TGraph objects for the plots
    fGraph = new TMultiGraph ("LVmonitor", "LV Monitor");

    //first the graphs for the Voltages
    TGraph* cCh1V = new TGraph();
    cCh1V->SetName ("cCh1V");
    cCh1V->SetTitle ("Channel 1 Voltage");
    cCh1V->SetLineColor (kBlue);
    cCh1V->SetLineWidth (3);
    TGraph* cCh2V = new TGraph();
    cCh2V->SetName ("cCh2V");
    cCh2V->SetTitle ("Channel 2 Voltage");
    cCh2V->SetLineColor (kRed);
    cCh2V->SetLineWidth (3);
    TGraph* cCh3V = new TGraph();
    cCh3V->SetName ("cCh3V");
    cCh3V->SetTitle ("Channel 3 Voltage");
    cCh3V->SetLineColor (kGreen);
    cCh3V->SetLineWidth (3);
    TGraph* cCh4V = new TGraph();
    cCh4V->SetName ("cCh4V");
    cCh4V->SetTitle ("Channel 4 Voltage");
    cCh4V->SetLineColor (kOrange);
    cCh4V->SetLineWidth (3);

    // add the graphs to the graph map so I can fill them
    fGraphVec.push_back (cCh1V);
    fGraphVec.push_back (cCh2V);
    fGraphVec.push_back (cCh3V);
    fGraphVec.push_back (cCh4V);

    //now the graphs for the currents
    TGraph* cCh1I = new TGraph();
    cCh1I->SetName ("cCh1I");
    cCh1I->SetTitle ("Channel 1 Current");
    cCh1I->SetLineColor (kBlue);
    cCh1I->SetLineWidth (3);
    TGraph* cCh2I = new TGraph();
    cCh2I->SetName ("cCh2I");
    cCh2I->SetTitle ("Channel 2 Current");
    cCh2I->SetLineColor (kRed);
    cCh2I->SetLineWidth (3);
    TGraph* cCh3I = new TGraph();
    cCh3I->SetName ("cCh3I");
    cCh3I->SetTitle ("Channel 3 Current");
    cCh3I->SetLineColor (kGreen);
    cCh3I->SetLineWidth (3);
    TGraph* cCh4I = new TGraph();
    cCh4I->SetName ("cCh4I");
    cCh4I->SetTitle ("Channel 4 Current");
    cCh4I->SetLineColor (kOrange);
    cCh4I->SetLineWidth (3);

    //add the graphs to the TMultiGraph for drawing
    fGraph->Add (cCh1V, "l");
    fGraph->Add (cCh2V, "l");
    fGraph->Add (cCh3V, "l");
    fGraph->Add (cCh4V, "l");

    //add the graphs to the TMultiGraph for drawing
    fGraph->Add (cCh1I, "l");
    fGraph->Add (cCh2I, "l");
    fGraph->Add (cCh3I, "l");
    fGraph->Add (cCh4I, "l");

    // add the graphs to the graph map so I can fill them
    fGraphVec.push_back (cCh1I);
    fGraphVec.push_back (cCh2I);
    fGraphVec.push_back (cCh3I);
    fGraphVec.push_back (cCh4I);

    if (fServer != nullptr)
        fServer->Register ("/", fGraph);

    for (auto& cGraph : fGraphVec)
        fServer->Register ("/IndividualGraphs/", cGraph);

    LOG (INFO) << "Graphs for Monitoring Plots Successfully created!";
}

void HMP4040Controller::clearGraphs()
{
    std::lock_guard<std::mutex> lock (fMemberMutex);

    if (fGraph) delete fGraph;

    fGraphVec.clear();
}

void HMP4040Controller::fillGraphs()
{
    double xmin, ymin, xmax, ymax;

    std::unique_lock<std::mutex> guard (fMemberMutex);

    for (int i = 0; i < 8; i++)
    {
        if (i == 0)
            fGraphVec.at (i)->ComputeRange (xmin, ymin, xmax, ymax);

        if (i < 4)
        {
            //voltage graphs
            fGraphVec.at (i)->SetPoint (fGraphVec.at (i)->GetN(), fValues.fTimestamp, fValues.fVoltages.at (i) );
        }
        else
        {
            //current graphs
            fGraphVec.at (i)->SetPoint (fGraphVec.at (i)->GetN(), fValues.fTimestamp, fValues.fCurrents.at (i - 4) );
        }

        this->prettifyGraph (fGraphVec.at (i) );
    }

    //this somehow works but the xaxis is not updating and moving
    fGraph->Draw ("A");
    fGraph->SetMaximum (6);
    fGraph->SetMinimum (0);
    //sliding 30 min time window
    fGraph->GetHistogram()->GetXaxis()->SetLimits (fValues.fTimestamp - 60 * 30, fValues.fTimestamp + 2 * 60);
    guard.unlock();
    //absolute time window
    //fGraph->GetHistogram()->GetXaxis()->SetLimits (xmin - 2 * 60, xmax + 2 * 60);
    fGraph->GetHistogram()->GetXaxis()->SetTitle ("Time [min]");
    fGraph->GetHistogram()->GetYaxis()->SetTitle ("Voltage [V], Current[A]");
    fGraph->GetHistogram()->GetXaxis()->SetTimeDisplay (1);
    //fGraph->GetHistogram()->GetXaxis()->SetNdivisions (503);
    fGraph->GetHistogram()->GetXaxis()->SetTimeFormat ("%H:%M");
    //gPad->Modified();
    //gPad->Update();
}

void HMP4040Controller::prettifyGraph (TGraph* pGraph)
{
    pGraph->GetXaxis()->SetLimits (fValues.fTimestamp - 60 * 30, fValues.fTimestamp + 2 * 60);
    pGraph->GetXaxis()->SetTitle ("Time [min]");
    pGraph->GetYaxis()->SetTitle ("Voltage [V], Current[A]");
    pGraph->GetXaxis()->SetTimeDisplay (1);
    //pGraph->GetXaxis()->SetNdivisions (503);
    pGraph->GetXaxis()->SetTimeFormat ("%H:%M");
}
