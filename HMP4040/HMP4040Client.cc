#include "HMP4040Client.h"
#ifdef __ZMQ__
using namespace Ph2_UsbInst;

void HMP4040Client::parseValueString (std::string& pValueString)
{
    std::string cDummyString;
    std::istringstream cStream (pValueString);

    if (! (cStream >> cDummyString >> fValues.fTimestamp >> fValues.fVoltages.at (0) >> fValues.fCurrents.at (0) >> fValues.fVoltages.at (1) >> fValues.fCurrents.at (1) >> fValues.fVoltages.at (2) >> fValues.fCurrents.at (2) >> fValues.fVoltages.at (3) >> fValues.fCurrents.at (3)  ) ) LOG (ERROR) << RED << "Error, could not read the received value string!" << RESET;
}

bool HMP4040Client::Reset()
{
    //std::string cRequest = "r";
    //std::string cReply;
    //this->SendRequestReceiveReply (cRequest, cReply);

    //if (cReply == RESETINST)
    //{
    //LOG (INFO) << "Reset processed successfully";
    //return true;
    //}
    //else return false;
    //LOG (DEBUG) << "Doing nothing!";
    return false;
}
bool HMP4040Client::StartMonitoring()
{
    std::string cRequest = "s";
    std::string cReply;
    bool cSuccess = this->SendRequestReceiveReply (cRequest, cReply);

    if (!cSuccess) return false;

    else if (cReply == STARTMONITOR)
    {
        LOG (INFO) << "Monitoring started successfully";
        return true;
    }
    else return false;
}
bool HMP4040Client::StopMonitoring()
{
    std::string cRequest = "e";
    std::string cReply;
    bool cSuccess = this->SendRequestReceiveReply (cRequest, cReply);

    if (!cSuccess) return false;
    else if (cReply == STOPMONITOR)
    {
        LOG (INFO) << "Monitoring stopped successfully";
        return true;
    }
    else return false;
}
bool HMP4040Client::PauseMonitoring()
{
    std::string cRequest = "p";
    std::string cReply;
    bool cSuccess = this->SendRequestReceiveReply (cRequest, cReply);

    if (!cSuccess) return false;
    else if (cReply == PAUSE)
    {
        LOG (INFO) << "Monitoring paused successfully";
        return true;
    }
    else return false;
}
bool HMP4040Client::ResumeMonitoring()
{
    std::string cRequest = "r";
    std::string cReply;
    bool cSuccess = this->SendRequestReceiveReply (cRequest, cReply);

    if (!cSuccess) return false;
    else if (cReply == RESUME)
    {
        LOG (INFO) << "Monitoring resumed successfully";
        return true;
    }
    else return false;
}
bool HMP4040Client::SetLogFileName (std::string pLogFileName)
{
    std::string cRequest = "o " + pLogFileName;
    std::string cReply;
    bool cSuccess = this->SendRequestReceiveReply (cRequest, cReply);

    if (!cSuccess) return false;
    else if (cReply.find (LOGFILE) != std::string::npos)
    {
        LOG (INFO) << "Changed log filename to " << pLogFileName;
        return true;
    }
    else return false;
}
bool HMP4040Client::SetConfigFileName (std::string pConfigFileName)
{
    std::string cRequest = "i " + pConfigFileName;
    std::string cReply;
    bool cSuccess = this->SendRequestReceiveReply (cRequest, cReply);

    if (!cSuccess) return false;
    else if (cReply.find (CONFIGFILE) != std::string::npos)
    {
        LOG (INFO) << "Loaded new config file: " << pConfigFileName;
        return true;
    }
    else return false;
}
bool HMP4040Client::Configure()
{
    std::string cRequest = "c";
    std::string cReply;
    bool cSuccess = this->SendRequestReceiveReply (cRequest, cReply);

    if (!cSuccess) return false;
    else if (cReply == CONFIGURE)
    {
        LOG (INFO) << "instrument configured successfully";
        return true;
    }
    else return false;
}
bool HMP4040Client::ToggleOutput (bool pState)
{
    std::string cRequest = pState ? "t on" : "t off";
    std::string cReply;
    bool cSuccess = this->SendRequestReceiveReply (cRequest, cReply);

    if (!cSuccess) return false;
    else if (cReply.find (OUTPUT) != std::string::npos)
    {
        std::string cState = pState ? "on" : "off";
        LOG (INFO) << "Switched global output " << cState;
        return true;
    }
    else return false;
}
bool HMP4040Client::GetLatestReadValues()
{
    std::string cRequest = "g";
    std::string cReply;
    bool cSuccess = this->SendRequestReceiveReply (cRequest, cReply);

    if (!cSuccess) return false;
    else if (cReply.find (VALUES) != std::string::npos)
    {
        this->parseValueString (cReply);
        LOG (INFO) << BLUE << "Received latest measured values" << RESET;
        std::cout << "Latest measured values (t=" << fValues.fTimestamp << "):" << std::endl;

        for (int i = 0; i < 4; i++)
            std::cout << "Channel" << i + 1 << ":\t" << fValues.fVoltages.at (i) << "V\t" << fValues.fCurrents.at (i) << "A" << std::endl;

        return true;
    }
    else return false;
}
bool HMP4040Client::MeasureValues()
{
    std::string cRequest = "m";
    std::string cReply;
    bool cSuccess = this->SendRequestReceiveReply (cRequest, cReply);

    if (!cSuccess) return false;
    else if (cReply.find (VALUES) != std::string::npos)
    {
        this->parseValueString (cReply);
        //LOG (INFO) << BLUE << "Measured values" << RESET;
        //std::cout << "Latest measured values (t=" << fValues.fTimestamp << "):" << std::endl;

        //for (int i = 0; i < 4; i++)
        //std::cout << "Channel" << i + 1 << ":\t" << fValues.fVoltages.at (i) << "V\t" << fValues.fCurrents.at (i) << "A" << std::endl;

        return true;
    }
    else return false;
}
bool HMP4040Client::Quit()
{
    std::string cRequest = "q";
    std::string cReply;
    bool cSuccess = this->SendRequestReceiveReply (cRequest, cReply);

    if (!cSuccess) return false;
    else if (cReply == QUIT)
    {
        LOG (INFO) << BOLDRED <<  "Server quit!" << RESET;
        return true;
    }
    else return false;
}

#endif
