# CMS Tracker Phase2 Upgrade USB Instrument Driver

this package provides an easy to use interface to serial or USB controlled laboratory equipment useful for activities related to the Ph2 Tracker Upgrade.

## Features

The library contains a low-level Driver/ directory that contains wrappers for Serial (Serial over USB) and USB (USBTMC) kernel drivers that can be used to communicate with any instrument supporting these protocols on Scientific Linux 6.

### Requirements
THttpServer and ZeroMC (C++) are strongly reccommended. You should
    sudo yum install root-net-http cppzmq-devel

### Serial Drivers

These are implemented using boost::asio functionality (thanks to J. Grossmann for digging it out) and support basic write (devwrite) and read(devread) methods taking std::strings as arguments and return values for SCPI commands.

Furthermore a "init_serial.sh" script is provided that shows how to load the correct kernel module for the HMP4040 4-Ch LV power supply.

In addition it is useful to create a new rule under /etc/udev/rules.d/10-hmp.rules like so:

    SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="ed72", MODE="666", GROUP="group", SYMLINK+="Hameg4040"

which will creae a symlink from /dev/ttyUSBx to /dev/Hameg4040 that is used by the SerialHandler class to identify the correct instrument.

### USB TMC drivers

The kernel module for this type of instrument needs to be compiled and loaded before any communication over USB TMC is possible. In order to do this, cd to the usbtmc/ directory and run make. Then run the usbtmc_load script with sudo privileges. This should create the /dev/usbtmcX node where X is the minor number of the device. To see which devices using this dirver are connected simply:

    cat /dev/usbtmc0

This file also contains the minor ID of the instruments. In order to talk to an instrument you can echo and cat to the /dev/usbtmcX file with X the minor ID for debug purposes. The UsbTmcHandler class should automatically pcik the correct minor ID for you if you specify the correct device name. 


## Example interfaces
An interface to the Hameg4040 LV power supply based on the SerialHandler (USBTMC not supported by this instrument) is provided in the HMP4040 Directory. It consists of a HMP4040Controller class that has methods to change all relevant settings of the instrument, toggle the output states, load a configuration from a xml file (located in settings/) etc. In addition it provides a threaded monitoring loop that, when enabled periodically reads the measured values on all channels and logs them in a file. If root is built with THttpServer support, monitoring graphs are also provided via JSroot on a local webserver.

The Controller is fully responsive, even when the monitoring loop is running so it is possible to change values and settings while the monitoring is on. 

This controller class is used to create a command line based interactive control application (lvSupervisor, run lvSupervisor --help to see all options). It can either be run in local mode where it accepts commands on the standard input or as a server where it accepts commands and can provide measurement values from a remote client via a tcp socket (using ZeroMQ library [sudo yum install zeromq-devel]) and in local mode.

The client is implemented as a separate class in HMP4040/ and can be included in any application that needs to talk to the lvSupervisor in server mode. A client command line tool with the same control options as the lvSupervisor is also provided called "testclient" (testclient --help to show the options) to test the functionality. 


Furthermore there is a very simple Ke2110Controller class in Ke2110/ along with an example config file in settings/Ke2110.xml. You should be able to integrate it in any other application just like the HMP Controller/Client with the exception that there is no client-server architecutre.

Every time a measurement is taken with the DMM, the value is stored with a timestamp in a log file (default DMM_log.txt)

## Requirements and Installation
The library tries to use standard C++11 (gcc > 4.7) where possible but since it will mostly be used on machines with a uhal installation which includes boost, some boost functionality is used for convenience. In addition, to use the client-server functionality the ZeroMQ messaging queue library needs to be installed (sudo yum install zeromq-devel).

So check out the repositor, cd into the directory and source the setup.sh script. This adds lib/ and bin/ directory to the path and points to the boost installation which comes with uHAL so it should work out of the box on any machine that can run Ph2_ACF. Running a simple 'make' should build all the libs and executables.

Both THttpServer and ZeroMQ support are optional and the basic features should work without these packages but it is advisable to install both packages to fully profit from the full functionality. 


- Install CERN ROOT version 5.34.32: [Instructions](http://root.cern.ch/drupal/content/installing-root-source) - make sure to use "fixed location installation" when building yourself. If root is installed on a CERN computer of virtual machine you can use:
       
        $> sudo yum install root
        $> sudo yum install root-net-http root-graf3d-gl root-physics libusb-devel root-montecarlo-eg root-graf3d-eve root-geom

## Running and linking against the library
The interface classes (HMP4040Controller and HMP4040Client should be self explanatory) and the options of the binaries can be dumped with the --help option just like in Ph2_ACF.
The command set available for the lvSupervisor and testclient are summarized below:

    Press one of the following keys:
        [h]elp:  print this list
        [s]tart the monitoring
        [e]nd the monitoring
        [q]uit this application (and the server in case of the client)
        [t]oggle "on/off" to switch the Outputs on/off
        [i]nitialize "filename" to (re-)load the config file
        [c]onfigure to apply the configuration from the current file
        [o]utput "filename" to change the name of the logfile - only valid after the monitoring has stopped and started again
        [r]eset to reset the instrument to default state
        [m]measure to force a read of the latest values and display them
        [g]et values to print the last measured values to std::cout
        [a]bort: only available in the client to kill the client without shutting down the server


The format of the configuration file for the HMP4040 is self-explanatory:

```xml

<?xml version='1.0' encoding='utf-8'?>

<InstrumentDescription>
    <HMP4040>
        <Channel id="1" voltage="1.23" current="1.2"/>
        <Channel id="2" voltage="3.3" current="1.4"/>
        <Channel id="3" voltage="5.0" current="1.1"/>
        <Channel id="4" voltage="5.0" current="1.3"/>
    </HMP4040>
</InstrumentDescription>

```

It is possible that this standalon repo will be made available as a plugin for the Ph2_ACF package similar to the Ph2_USBAntennaDriver with Ph2_ACF Makefiles automatically checking for presence of the USBInstLib and accordingly making available the interfaces but for the time being it is recommended to install separately.

The library extensively used the easylogging++ library which is included in /Utils. This allows do dump output to std::cout and various logfiles depending on the logging level. Logfiles are usually found under /tmp and the behavior of the logger can be configured by changing the settings/logger.conf file.


```sh
* GLOBAL:
    FORMAT                  =   "%datetime{%d.%M.%Y %H:%m} |%thread|%levshort| %msg"
    FILENAME                =   "/tmp/logs/Ph2_USBInst.log"
    ENABLED                 =   true
    TO_FILE                 =   true
    TO_STANDARD_OUTPUT      =   true
    MILLISECONDS_WIDTH      =   3
    PERFORMANCE_TRACKING    =   false
    MAX_LOG_FILE_SIZE       =   2097152 ## Throw log files away after 2MB
* DEBUG:
    FORMAT                  =   "%datetime{%d.%M.%Y %H:%m} |%thread|%levshort|%file|%line| %msg"
    FILENAME                =   "/tmp/logs/Ph2_USBInst_DEBUG.log"
    TO_STANDARD_OUTPUT      =   true
    ENABLED                 =   true ## We will set it to false after development completed
* WARNING:
    FILENAME                =   "/tmp/logs/Ph2_USBInst-%datetime{%H:%m}"
* VERBOSE:
    FORMAT                  =   "%datetime{%d/%M/%y} | %level-%vlevel | %msg"
## Error logs
* ERROR:
    ENABLED                 =   false
    FILENAME                =   "/tmp/logs/Ph2_USBInst_ERROR.log"
* FATAL:
    ENABLED                 =   false
```


## Disclaimer
This library has been developed by personal use for specific testing applications in mind although it might be useful for other members of the project. Feel free to fork & modify according to your needs. Merge request will be reviewed and eventually merged but it is not planned to keep this project alive actively. New features will only be added as needed.

No active support will be provided.
