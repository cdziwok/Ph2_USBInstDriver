
/*

    \file                          SerialHandler.h
    \brief                         Wrapper class to communicate with USB instruments via usbtmc and SCPI
    \author                        Georg Auzinger
    \version                       1.0
    \date                          22/09/2016
    Support :                      mail to : georg.auzinger@SPAMNOT.cern.ch

 */

#ifndef _SERIALHANDLER__
#define _SERIALHANDLER__
#include <iostream>
#include <chrono>
#include <thread>
#include <memory>
#include <cstdlib>
//#include <system>
#include <string>
#include "AsyncSerial.h"
#include "BufferedAsyncSerial.h"
#include "../Utils/easylogging++.h"
#include "../Utils/ConsoleColor.h"

namespace Ph2_UsbInst {

    class SerialHandler
    {
      public:
        SerialHandler (std::string pDevName);

        // Destructor needs to close fFile!
        ~SerialHandler()
        {
            //LOG(INFO) << "Closing open serial port." ; fSerial->close();
        }

        void SetCheckLevel(bool pCheck){ fCheck = pCheck; };
        void Multithreading(bool pMultex){ fMultiThreaded = pMultex ;}
        // generic write method that accepts SCPI command as string!
        std::string devwrite (std::string pMsg );
        
        // generic read method that accepts SCPI command as string!
        std::string devread (uint32_t pCounter = 1000 );

      private:
        bool fCheck; 
        bool fMultiThreaded; 
        const std::string lf = "\n";
        std::shared_ptr<BufferedAsyncSerial> fSerial;

        void emptyBuffer()
        {
            std::string cReadStr = "a";
            uint32_t cCounter = 0;

            while (!cReadStr.empty() )
            {
                if (cCounter == 100000 ) break;

                std::this_thread::sleep_for (std::chrono::milliseconds (4) );
                cReadStr = this->fSerial->readStringUntil (lf);
                cCounter++;
            }
        }
    };
}





#endif
