#include "SerialHandler.h"

using namespace Ph2_UsbInst;

SerialHandler::SerialHandler (std::string pDevName)
{
    unsigned int cBaudRate;
    std::string cDevname;
    boost::asio::serial_port_base::parity cParity;
    boost::asio::serial_port_base::character_size cCharSize;
    boost::asio::serial_port_base::flow_control cFlowCtrl;
    boost::asio::serial_port_base::stop_bits cStopBits;

    //the actual shared memory object
    fSerial = std::make_shared<BufferedAsyncSerial>();
    fCheck = false;

    if (pDevName == "HMP4040")
    {
        cParity = boost::asio::serial_port_base::parity (boost::asio::serial_port_base::parity::none);
        cCharSize = static_cast<boost::asio::serial_port_base::character_size> (8);
        cFlowCtrl = boost::asio::serial_port_base::flow_control (boost::asio::serial_port_base::flow_control::none);
        cStopBits = boost::asio::serial_port_base::stop_bits (boost::asio::serial_port_base::stop_bits::one);
        cDevname = "/dev/Hameg4040";
        cBaudRate = 9600;//115200;
        //lf = "\n";
        LOG (INFO) << GREEN << "Settings for Serial Handler for device " << pDevName << " found - trying to initialize Interface!" << RESET;
        //fMultiThreaded = false;
    }
    // adding options for Arduino Nano
    else if ( pDevName == "ArduinoNano")
    {
        cParity = boost::asio::serial_port_base::parity (boost::asio::serial_port_base::parity::none);
        cCharSize = static_cast<boost::asio::serial_port_base::character_size> (8);
        cFlowCtrl = boost::asio::serial_port_base::flow_control (boost::asio::serial_port_base::flow_control::none);
        cStopBits = boost::asio::serial_port_base::stop_bits (boost::asio::serial_port_base::stop_bits::one);
        cDevname = "/dev/ArduinoNano";
        cBaudRate = 9600;
        //lf = "\n";
        LOG (INFO) << GREEN << "Settings for Serial Handler for device " << pDevName << " found - trying to initialize Interface!" << RESET;
        fCheck = false;
        fMultiThreaded = false;
    }
    else
        LOG (ERROR) << RED << "Instrument currently not supported!" << RESET;

    try
    {
        if ( fSerial->isOpen() )
        {
            LOG (INFO) << "Closing open serial port." ;
            fSerial->close();
        }

        fSerial->open (cDevname, cBaudRate, cParity, cCharSize, cFlowCtrl, cStopBits);

        LOG (DEBUG) << "Going to clear the buffer";
        if ( fMultiThreaded ) this->emptyBuffer();

        LOG (DEBUG) << "Not going to clear the buffer";

    }
    catch (boost::system::system_error& cErr)
    {
        LOG (ERROR) << RED << "Catch boost::system error, serial connection not open!\t" << cErr.what();

        if ( pDevName == "HMP4040" )
        {
            LOG (ERROR) << "Make sure that the file /dev/Hameg4040 exists - if not maybe add a rule to /etc/udev/rules.d/ containing: " ;
            LOG (ERROR) << "SUBSYSTEM == \"tty\", ATTRS{idVendor} == \"0403\", ATTRS{idProduct} == \"ed72\", MODE = \"666\", GROUP = \"group\", SYMLINK += \"Hameg4040\"" ;
            LOG (ERROR) << "and re-plug the instrument!" << RESET ;
        }
        else if ( pDevName == "ArduinoNano")
        {
            LOG (ERROR) << "Make sure that the file /dev/ArduinoNano exists - if not maybe add a rule to /etc/udev/rules.d/ containing: " ;
            LOG (ERROR) << "SUBSYSTEM == \"tty\", ATTRS{idVendor} == \"1a86\", ATTRS{idProduct} == \"7523\", MODE = \"666\", GROUP = \"group\", SYMLINK += \"ArduinoNano\"" ;
            LOG (ERROR) << "and re-plug the instrument!" << RESET ;
        }

        exit (1);
    }
}

std::string SerialHandler::devwrite (std::string pMsg  )
{
    LOG (INFO) << BOLDBLUE << "Writing : " << pMsg << " to instrument." << RESET;
    if ( fCheck ) this->fSerial->writeString ("*CLS" + lf);

    if ( fMultiThreaded ) this->fSerial->writeString (pMsg);
    else
        this->fSerial->writeSimple ( pMsg ) ;

    if ( fCheck ) this->fSerial->writeString ("*OPC?" + lf);

    std::string cReadString = "";

    while (cReadString.empty() )
    {
        std::this_thread::sleep_for (std::chrono::nanoseconds (100) );

        if ( fMultiThreaded) cReadString = this->fSerial->readStringUntil (lf);
        else cReadString = this->fSerial->readSimpleStringUntil (lf);

        //if ( !cReadString.empty() ) LOG (DEBUG) << "Write reply: " << cReadString << std::endl;
    }

    if ( fMultiThreaded) this->emptyBuffer();

    return cReadString;
}

// generic read method that accepts SCPI command as string!
std::string SerialHandler::devread (uint32_t pCounter /*= 1000*/)
{
    std::string cReadString = "";
    uint32_t cCounter = 0;

    while (cReadString.empty() )
    {
        if (cCounter == pCounter) break;

        std::this_thread::sleep_for (std::chrono::nanoseconds (100) );

        if ( fMultiThreaded) cReadString = this->fSerial->readStringUntil (lf);
        else cReadString = this->fSerial->readSimpleStringUntil (lf);

        cCounter++;
        //if ( cCounter%10000 == 0 || !cReadString.empty() ) LOG (DEBUG) << "Read: " << cReadString << " on counter == " << +cCounter << " !" << std::endl;
        //if (!cReadString.empty() ) LOG (DEBUG) << "Read: " << cReadString << std::endl;
    }

    return cReadString;
}
